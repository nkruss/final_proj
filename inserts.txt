INSERT INTO Employee (ssn, fname, lname, age) 
VALUES
	(1,"Damian", "Lillard", 31),
    (2,"CJ", "McCollumn", 31),
    (3, "Jusuf", "Nurkic", 27),
    (4, "Chauncey", "Billups", NULL),
    (5, "Roy", "Rogers", NULL),
    (6, "Anfernee", "Simmons", 22),
    (7, "Nassir", "Little", 21),
    (8, "Steve", "Kerr", NULL),
    (9, "Ron", "Adams", NULL),
    (10, "Stephen", "Curry", 33),
    (11, "Klay", "Thompson", 31),
    (12, "Draymon", "Green", 31),
    (13, "Kevon", "Looney", 25),
    (14, "Andre", "Iguodala", 37),
    (15, "Monty", "Williams", NULL),
    (16, "Willie", "Green", NULL),
    (17, "Devin", "Booker", 25),
    (18, "Chris", "Paul", 36),
    (19, "Deandre", "Ayton", 23),
    (20, "Cameron", "Johnson", 24),
    (21, "Jae", "Crowder", 32),
    (22, "Michael", "Malone", NULL),
    (23, "Andrew", "Munson", NULL),
    (24, "Nikola", "Jokic", 27),
    (25, "Jamel", "Murray", 25),
    (26, "Michael", "Porter", 21),
    (27, "Will", "Barton", 28),
    (28, "Austin", "Rivers", 31),
    (29, "Jason", "Kidd", NULL),
    (30, "Darell", "Armstrong", NULL),
    (31, "Luka", "Doncic", 22),
    (32, "Kristaps", "Porzingis", 24),
    (33, "Tim", "Hardaway Jr", 30),
    (34, "Boban", "Marjanovic", 30),
    (35, "Trey", "Burke", 28);

INSERT INTO Teams (team_id, team_name, mascot, head_coach_ssn)
VALUES
	(1, "Warriors", "Thunder", NULL),
    (2, "Suns", "The Suns Gorilla", NULL),
    (3, "Nuggets", "Rocky the Mountain Lion", NULL),
    (4, "Mavericks", "MavsMan and the Champ", NULL),
    (5, "Blazers", "Blaze the Trail Cat", NULL);

INSERT INTO Players (player_ssn, team_id, rank, jersey_num, score_avg, salary)
VALUES
	(1,  5, 94, 0,  27,  39344900),
    (2,  5, 85, 3,  23,  30864198),
    (3,  5, 81, 27, 11,  12000000),
    (6,  5, 75, 1,  12,  3938818),
    (7,  5, 73, 9,  8,   2316240),
    (10, 1, 96, 30, 28,  45780966),
    (11, 1, 87, 11, 22,  37980720),
    (12, 1, 80, 23, 9,   24026712),
    (13, 1, 73, 5,  6,   5178572),
    (14, 1, 73, 9,  12,  2641691),
    (17, 2, 90, 1,  23,  31650600),
    (18, 2, 90, 3,  18,  30800000),
    (19, 2, 86, 22, 16,  12632950),
    (20, 2, 77, 23, 9,   4437000), 
    (21, 2, 77, 99, 10,  9720900),
    (24, 3, 95, 15, 19,  31579390),
    (25, 3, 85, 27, 16,  29467800),
    (26, 3, 84, 1,  14,  5258735),
    (27, 3, 77, 5,  11,  15625000),
    (28, 3, 74, 25, 9,   2401537),
    (31, 4, 94, 77, 26,  10174391),
    (32, 4, 84, 6,  19,  31650600),
    (33, 4, 79, 11, 14,  21306816),
    (34, 4, 74, 51, 6,   3500000),
    (35, 4, 75, 3,  10,  3150000);

INSERT INTO Starters (player_ssn)
VALUES
	(1),
    (2),
    (3),
    (10),
    (11),
    (12),
    (17),
    (18),
    (19),
    (24),
    (25),
    (26),
    (31),
    (32),
    (33);
    
INSERT INTO Bench (player_ssn)
VALUES
	(6),
    (7),
    (13),
    (14),
    (20),
    (21),
    (27),
    (28),
    (34),
    (35);
    
INSERT INTO Coaches (coach_ssn, team_id)
VALUES
	(4, 5),
    (5, 5),
    (8, 1),
    (9, 1),
    (15, 2),
    (16, 2),
    (22, 3),
    (23, 3),
    (29, 2),
    (30, 2);
    
UPDATE `NBA`.`Teams` SET `head_coach_ssn` = '8' WHERE (`team_id` = '1');
UPDATE `NBA`.`Teams` SET `head_coach_ssn` = '15' WHERE (`team_id` = '2');
UPDATE `NBA`.`Teams` SET `head_coach_ssn` = '22' WHERE (`team_id` = '3');
UPDATE `NBA`.`Teams` SET `head_coach_ssn` = '29' WHERE (`team_id` = '4');
UPDATE `NBA`.`Teams` SET `head_coach_ssn` = '4' WHERE (`team_id` = '5');
    
INSERT INTO Games (home_team_id, away_team_id, game_date, home_team_score, away_team_score)
VALUES
	(1, 2, '2021-10-01', 95, 100),
    (4, 1, '2021-10-02', 82, 95),
    (5, 3, '2021-10-03', 117, 97), 
    (1, 3, '2021-10-04', 93, 107),
    (2, 5, '2021-10-06', 110, 111),
    (5, 4, '2021-10-07', 98, 104),
    (4, 3, '2021-10-07', 109, 91),
    (3, 2, '2021-10-08', 92, 98);
    
INSERT INTO Fans (fan_id, fname, lname, fav_team_id, fav_player_ssn)
VALUES
	(1, "John", "Doe", 5, 1),
    (2, "Dave", "Smith", 2, 17),
    (3, "Ellie", "Cooper", 5, 1),
    (4, "Bret", "Kiln", 1, 10),
    (5, "Joe", "Kine", 1, 11),
    (6, "Elyse", "Graves", 3, 25),
    (7, "Steve", "Green", 2, 18),
    (8, "Jamie", "Harper", 4, 31),
    (9, "Anna", "James", 4, 33),
    (10, "Grace", "Billow", 5, 3);
    
INSERT INTO GamesAttended (Fan_fan_id, home_team_id, away_team_id, game_date)
VALUES
	(2, 1, 2, '2021-10-01'),
    (7, 1, 2, '2021-10-01'),
    (4, 1, 2, '2021-10-01'),
    (5, 4, 1, '2021-10-02'),
    (4, 4, 1, '2021-10-02'),
    (8, 4, 1, '2021-10-02'),
    (9, 4, 1, '2021-10-02'),
    (10, 5, 3,'2021-10-03');