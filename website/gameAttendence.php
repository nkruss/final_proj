<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <title>CIS 451 Final Project - Game Attendence</title>
  <link href="css/table.css" rel="stylesheet">
  </head>
  
  <h3>CIS 451 Final Project - Game Attendence</h3>
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
  
$date = $_POST['date'];

$date = mysqli_real_escape_string($conn, $date);

$query = "SELECT CONCAT(t2.team_name, ' AT ', t.team_name) AS game, g.game_date, 
          CONCAT(f.fname, ' ', f.lname) AS fan, t3.team_name AS supports
          FROM GamesAttended g JOIN Fans f ON (g.Fan_fan_id = f.fan_id)
            JOIN Teams t ON (t.team_id = g.home_team_id)
            JOIN Teams t2 ON (t2.team_id = g.away_team_id)
            JOIN Teams t3 ON (t3.team_id = f.fav_team_id)
          WHERE g.game_date = ";
$query = $query."'".$date."' ORDER BY t.team_id, t3.team_name;";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Game </th> 
    <th style='text-decoration:underline'>Date </th> 
    <th style='text-decoration:underline'>Fan </th> 
    <th style='text-decoration:underline'>Fan's Team </th> </tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[game]\t</th> 
          <th>$row[game_date]\t</th> 
          <th>$row[fan]\t</th> 
          <th>$row[supports]\t</th>";
    print "</tr>";
  }
print "</table>";



mysqli_free_result($result);


mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>