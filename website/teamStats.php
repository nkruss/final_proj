<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <link href="css/table.css" rel="stylesheet"> 
  <title>CIS 451 Final Project - Teams Stats</title>
  </head>
  
  <h3>CIS 451 Final Project - Teams Stats</h3>
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
  
$stat = $_POST['stat'];

$stat = mysqli_real_escape_string($conn, $stat);

$query = "SELECT t.team_name, SUM(p.$stat) AS total_stats
        FROM Teams t JOIN Players p USING (team_id)
        GROUP BY t.team_name
        ORDER BY total_stats DESC;";
?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Team </th>  
    <th style='text-decoration:underline'>Total $stat</th> </tr>";

while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[team_name]\t</th> <th>$row[total_stats]\t</th>";
    print "</tr>";
  }
print "</table>";

mysqli_free_result($result);


mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>