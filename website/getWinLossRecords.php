<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <link href="css/table.css" rel="stylesheet">
  <title>CIS 451 Final Project - Get Win Loss Records For All Teams</title>
  </head>
  
  <body bgcolor="white">
  <h3>CIS 451 Final Project - Get Win Loss Records For All Teams</h3>
  
  
  <hr>
  
  
<?php

$query = "SELECT team_name, IFNULL(games_won, 0) AS wins, IFNULL(games_lost, 0) AS losses
          FROM 
            (SELECT t.team_name, COUNT(*) as games_won
            FROM Games g JOIN Teams t ON (g.home_team_id = t.team_id)
            WHERE home_team_score > away_team_score
            GROUP BY t.team_name) q1
              LEFT JOIN
              (SELECT t.team_name, COUNT(*) as games_lost
            FROM Games g JOIN Teams t ON (g.away_team_id = t.team_id)
            WHERE away_team_score > home_team_score
            GROUP BY t.team_name) q2 
              USING (team_name)
          UNION
          SELECT team_name, IFNULL(games_won, 0) AS wins, IFNULL(games_lost, 0) AS losses
          FROM 
            (SELECT t.team_name, COUNT(*) as games_won
            FROM Games g JOIN Teams t ON (g.home_team_id = t.team_id)
            WHERE home_team_score > away_team_score
            GROUP BY t.team_name) q1
              RIGHT JOIN
              (SELECT t.team_name, COUNT(*) as games_lost
            FROM Games g JOIN Teams t ON (g.away_team_id = t.team_id)
            WHERE away_team_score > home_team_score
            GROUP BY t.team_name) q2 
              USING (team_name)
            ORDER BY wins - losses DESC, wins DESC";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Team </th> 
    <th style='text-decoration:underline'>Number of Wins</th> 
    <th style='text-decoration:underline'>Number of Losses</th></tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[team_name]\t</th> <th>$row[wins]\t</th> <th>$row[losses]\t</th>";
    print "</tr>";
  }
print "</table>";

mysqli_free_result($result);


mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>