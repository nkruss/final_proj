<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <link href="css/table.css" rel="stylesheet">
  <title>CIS 451 Final Project - Team Scoring Average</title>
  </head>
  
  <body bgcolor="white">
  <h3>CIS 451 Final Project - Team Scoring Average</h3>
  
  
  <hr>
  
  
<?php
  
$team = $_POST['team'];

$team = mysqli_real_escape_string($conn, $team);

$query = "SELECT team_name, AVG(avg_score) AS avg_score
          FROM (
                SELECT t.team_name, AVG(g.home_team_score) AS avg_score
                FROM Teams t JOIN Games g ON (t.team_id = g.home_team_id)
                GROUP BY t.team_id
                UNION
                SELECT t.team_name, AVG(g.away_team_score) AS avg_score
                FROM Teams t JOIN Games g ON (t.team_id = g.away_team_id)
                GROUP BY t.team_id
                ) q
          WHERE team_name = ";
$query = $query."'".$team."' GROUP BY team_name;";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Team </th> 
    <th style='text-decoration:underline'>Average Score</th> </tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[team_name]\t</th> <th>$row[avg_score]\t</th>";
    print "</tr>";
  }
print "</table>";

mysqli_free_result($result);


mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>