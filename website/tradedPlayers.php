<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <title>CIS 451 Final Project - Team Trades</title>
  <link href="css/table.css" rel="stylesheet">
  </head>
  
  <h3> CIS 451 Final Project - Team Trades </h3>
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
  
$team = $_POST['team'];

$team = mysqli_real_escape_string($conn, $team);

$query = "SELECT CONCAT(e.fname, ' ', e.lname) AS player, tr.trade_id, 'traded' AS trade_action
          FROM Trades tr JOIN Players p USING (player_ssn)
            JOIN Employee e ON (p.player_ssn = e.ssn)
            JOIN Teams t ON (tr.orig_team_id = t.team_id)
          WHERE t.team_name = ";
$query = $query."'".$team."'";

$query = $query."UNION
                SELECT CONCAT(e2.fname, ' ', e2.lname) AS player, tr2.trade_id, 'recieved' AS trade_action
                FROM Trades tr2 JOIN Players p2 USING (player_ssn)
                  JOIN Employee e2 ON (p2.player_ssn = e2.ssn)
                  JOIN Teams t2 ON (tr2.new_team_id1 = t2.team_id)
                WHERE t2.team_name = ";
$query = $query."'".$team."' ORDER BY trade_id, trade_action ;";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Player </th>
    <th style='text-decoration:underline'>Trade ID </th> 
    <th style='text-decoration:underline'>Trade Action</th> </tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player]\t</th> <th>$row[trade_id]\t</th> <th>$row[trade_action]\t</th>";
    print "</tr>";
  }
print "</table>";



mysqli_free_result($result);
mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>