<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <link href="css/table.css" rel="stylesheet">  
  <title>CIS 451 Final Project - Player Value</title>
  </head>
  
  <h3> CIS 451 Final Project - Player Value </h3>
  <body bgcolor="white">
  
  <hr>
  
  
<?php
  
$value = $_POST['value'];

$value = mysqli_real_escape_string($conn, $value);

$query = "SELECT CONCAT(e.fname, ' ', e.lname) AS player, p.$value AS value, t.team_name
          FROM Employee e JOIN Players p ON (e.ssn = p.player_ssn)
          JOIN Teams t USING (team_id)
          WHERE p.$value = (SELECT MAX($value) FROM Players p_in WHERE p.team_id = p_in.team_id)
          ORDER BY p.$value DESC";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Player </th> 
    <th style='text-decoration:underline'>$value </th> 
    <th style='text-decoration:underline'>Team</th> </tr>";

while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player]\t</th> <th>$row[value]\t</th> <th>$row[team_name]\t</th>";
    print "</tr>";
  }
print "</table>";


mysqli_free_result($result);


mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>