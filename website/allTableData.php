<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <link href="css/table.css" rel="stylesheet">
  <title>CIS 451 Final Project - Database Contents </title>
  </head>
  
  <h3>CIS 451 Final Project - Database Contents </h3>
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
$query_teams = "SELECT *
                  FROM Teams";

$query_employee = "SELECT *
                  FROM Employee";

$query_coaches = "SELECT *
                  FROM Coaches";

$query_players = "SELECT *
                  FROM Players";

$query_starters = "SELECT *
                  FROM Starters";

$query_bench = "SELECT *
                  FROM Bench";

$query_trades = "SELECT *
                  FROM Trades";

$query_fans = "SELECT *
                  FROM Fans";   

$query_games = "SELECT *
                  FROM Games
                  ORDER BY game_date";               

$query_gamesAttended = "SELECT *
                        FROM GamesAttended
                        ORDER BY game_date";
?>

<?php
$result_teams = mysqli_query($conn, $query_teams)
or die(mysqli_error($conn));

$result_employee = mysqli_query($conn, $query_employee)
or die(mysqli_error($conn));

$result_players = mysqli_query($conn, $query_players)
or die(mysqli_error($conn));

$result_coaches = mysqli_query($conn, $query_coaches)
or die(mysqli_error($conn));

$result_starters = mysqli_query($conn, $query_starters)
or die(mysqli_error($conn));

$result_bench = mysqli_query($conn, $query_bench)
or die(mysqli_error($conn));

$result_trades = mysqli_query($conn, $query_trades)
or die(mysqli_error($conn));

$result_fans = mysqli_query($conn, $query_fans)
or die(mysqli_error($conn));

$result_games = mysqli_query($conn, $query_games)
or die(mysqli_error($conn));

$result_gamesAttended = mysqli_query($conn, $query_gamesAttended)
or die(mysqli_error($conn));

// ----------------------------------

print "<pre>";
print "-- TEAMS -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>team_id </th> 
    <th style='text-decoration:underline'>team_name </th> 
    <th style='text-decoration:underline'>mascot</th> 
    <th style='text-decoration:underline'>head_coach_ssn</th></tr>";
while($row = mysqli_fetch_array($result_teams, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[team_id]\t</th> <th>$row[team_name]</th> <th>$row[mascot]</th> <th>$row[head_coach_ssn]</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- EMPLOYEE -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>ssn </th> 
    <th style='text-decoration:underline'>fname </th> 
    <th style='text-decoration:underline'>lname</th> 
    <th style='text-decoration:underline'>age</th></tr>";
while($row = mysqli_fetch_array($result_employee, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[ssn]\t</th> <th>$row[fname]\t</th> <th>$row[lname]\t</th> <th>$row[age]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- PLAYERS -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>player_ssn </th> 
    <th style='text-decoration:underline'>team_id </th> 
    <th style='text-decoration:underline'>rank </th> 
    <th style='text-decoration:underline'>jersey_num </th>
    <th style='text-decoration:underline'>score_avg </th>
    <th style='text-decoration:underline'>salary </th></tr>";
while($row = mysqli_fetch_array($result_players, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player_ssn] \t</th>
          <th>$row[team_id] \t</th>
          <th>$row[rank]\t</th> 
          <th>$row[jersey_num] \t</th>
          <th>$row[score_avg] \t</th>
          <th>$row[salary]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- COACHES -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>coach_ssn </th>  
    <th style='text-decoration:underline'>team_id</th> </tr>";
while($row = mysqli_fetch_array($result_coaches, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[coach_ssn]\t</th> <th>$row[team_id]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- STARTERS -- \n";
print "</pre>";
print  "<table>";
print  "<th style='text-decoration:underline'>player_ssn</th> </tr>";
while($row = mysqli_fetch_array($result_starters, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player_ssn]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- Bench -- \n";
print "</pre>";
print  "<table>";
print  "<th style='text-decoration:underline'>player_ssn</th> </tr>";
while($row = mysqli_fetch_array($result_bench, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player_ssn]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- TRADES -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>trade_id </th> 
    <th style='text-decoration:underline'>player_ssn </th> 
    <th style='text-decoration:underline'>orig_team_id </th> 
    <th style='text-decoration:underline'>new_team_id </th></tr>";
while($row = mysqli_fetch_array($result_trades, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[trade_id] \t</th>
          <th>$row[player_ssn] \t</th>
          <th>$row[orig_team_id] \t</th>
          <th>$row[new_team_id1]\t</th>";
    print "</tr>";
  }
print "</table>";


print "<pre>";
print "-- FANS -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>fan_id </th> 
    <th style='text-decoration:underline'>fname </th> 
    <th style='text-decoration:underline'>lname </th> 
    <th style='text-decoration:underline'>fac_team_id </th>
    <th style='text-decoration:underline'>fac_player_ssn </th></tr>";
while($row = mysqli_fetch_array($result_fans, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[fan_id] \t</th>
          <th>$row[fname] \t</th>
          <th>$row[lname] \t</th>
          <th>$row[fav_team_id] \t</th>
          <th>$row[fav_player_ssn]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- GAMES -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>home_team_id </th> 
    <th style='text-decoration:underline'>away_team_id </th> 
    <th style='text-decoration:underline'>game_date </th> 
    <th style='text-decoration:underline'>home_team_score </th>
    <th style='text-decoration:underline'>away_team_score </th></tr>";
while($row = mysqli_fetch_array($result_games, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[home_team_id] \t</th>
          <th>$row[away_team_id] \t</th>
          <th>$row[game_date] \t</th>
          <th>$row[home_team_score] \t</th>
          <th>$row[away_team_score]\t</th>";
    print "</tr>";
  }
print "</table>";

print "<pre>";
print "-- GAMES ATTENDED -- \n";
print "</pre>";
print  "<table>";
print  "<tr> <th style='text-decoration:underline'>fan_id </th> 
    <th style='text-decoration:underline'>home_team_id </th> 
    <th style='text-decoration:underline'>away_team_id </th> 
    <th style='text-decoration:underline'>game_date </th></tr>";
while($row = mysqli_fetch_array($result_gamesAttended, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[Fan_fan_id] \t</th>
          <th>$row[home_team_id] \t</th>
          <th>$row[away_team_id] \t</th>
          <th>$row[game_date] \t</th>";
    print "</tr>";
  }
print "</table>";

mysqli_free_result($result_teams);
mysqli_free_result($result_employee);
mysqli_free_result($result_coaches);
mysqli_free_result($result_players);
mysqli_free_result($result_starters);
mysqli_free_result($result_bench);
mysqli_free_result($result_trades);
mysqli_free_result($result_fans);
mysqli_free_result($result_games);
mysqli_free_result($result_gamesAttended);

mysqli_close($conn);
?>
 
</body>
</html>