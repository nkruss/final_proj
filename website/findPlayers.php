<?php

include('connectionData.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
<head>
  <link href="css/table.css" rel="stylesheet">
  <title>CIS 451 Final Project - Players on a team</title>
  </head>
  
  <h3>CIS 451 Final Project - Players on a team </h3>
  <body bgcolor="white">
  
  <hr>
  
  
<?php
  
$team = $_POST['team'];

$team = mysqli_real_escape_string($conn, $team);


$query_starters = "SELECT CONCAT(e.fname, ' ', e.lname) AS player, p.rank, p.jersey_num
		FROM Employee e JOIN Players p ON (e.ssn = p.player_ssn)
      JOIN Teams t USING (team_id)
      JOIN Starters s USING (player_ssn)
    WHERE t.team_name = ";
$query_starters = $query_starters."'".$team."' ORDER BY p.rank DESC;";

$query_bench = "SELECT CONCAT(e.fname, ' ', e.lname) AS player, p.rank, p.jersey_num
      FROM Employee e JOIN Players p ON (e.ssn = p.player_ssn)
      JOIN Teams t USING (team_id)
      JOIN Bench b USING (player_ssn)
      WHERE t.team_name = ";
$query_bench = $query_bench."'".$team."' ORDER BY p.rank DESC;";
?>

<p>
The query:
<p>
<?php
print $query_starters;
print "\n";
print $query_bench;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result_starters = mysqli_query($conn, $query_starters)
or die(mysqli_error($conn));

$result_bench = mysqli_query($conn, $query_bench)
or die(mysqli_error($conn));

print "<pre>";
print "-- STARTERS -- \n";
print "</pre>";

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Player </th> 
    <th style='text-decoration:underline'>Rank </th> 
    <th style='text-decoration:underline'>Jersey Number</th> </tr>";
while($row = mysqli_fetch_array($result_starters, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player]\t</th> <th>$row[rank]\t</th> <th>$row[jersey_num]\t</th>";
    print "</tr>";
  }
print "</table>";

// -------------------------
print "<pre>";
print "-- BENCH -- \n";
print "</pre>";

print  "<table>";
print  "<tr> <th style='text-decoration:underline'>Player </th> 
    <th style='text-decoration:underline'>Rank </th> 
    <th style='text-decoration:underline'>Jersey Number</th> </tr>";
while($row = mysqli_fetch_array($result_bench, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[player]\t</th> <th>$row[rank]\t</th> <th>$row[jersey_num]\t</th>";
    print "</tr>";
  }
print "</table>";


mysqli_free_result($result_starters);
mysqli_free_result($result_bench);


mysqli_close($conn);

?>

<p>
<hr>

<p> 
 
</body>
</html>
	  